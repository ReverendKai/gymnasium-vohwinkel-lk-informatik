package projekt_03_methoden_und_variablen;

public class MethodenUndVariablen_Musterloesung {

	static int datenfeld;
	
	public static void main(String[] args) {
		schreibeVerdoppeltInDatenfeld(5);
		int x = addiereZuDatenfeld(12);
		System.out.println(x);
		
		String s = "";
		s = vertauscheBuchstaben("abcdef"); // soll "badcfe" zurückgeben
		System.out.println(s);
		s = vertauscheBuchstaben("ghijklm"); // soll "hgjilkm" zurückgeben
		System.out.println(s);
	}
	
	public static void schreibeVerdoppeltInDatenfeld(int zuVerdoppelndeZahl) {
		datenfeld = zuVerdoppelndeZahl*2;
	}
	
	/**
	 * Die Aufgabenstellung ist nicht ganz eindrutig, muss ich zugeben. :-)
	 * Es ist nicht klar, ob das Datenfeld beim Aufruf der Methode verändert werden soll oder nicht.
	 * Ich notiere hier beide Lösungen:
	 */
	public static int addiereZuDatenfeld(int summand) {
		// Datenfeld bleibt unverändert:
		return datenfeld + summand;
		
		// Datenfeld wird verändert:
		//datenfeld = datenfeld + summand;
		//return datenfeld;
	}
	
	public static String vertauscheBuchstaben(String eingabe) {
		String ret = "";
		for (int i=0; i<eingabe.length()-1; i = i + 2) {
			ret = ret + eingabe.charAt(i+1) + eingabe.charAt(i);
		}
		
		// Anhängen des letzten Buchstabens, wenn eingabe eine ungerade Länge hat:
		// Man kan sowohl die Bedingung, als auch die Art, wie man den Buchstaben anhängt, auf
		// ganz viele verschiedene Weisen machen. Ich führe mal zwei auf:
		// Variante 1:
		if (ret.length() != eingabe.length()) {
			ret = ret + eingabe.charAt(eingabe.length()-1);
		}
		// Variante 2:
		if (eingabe.length()%2 == 1) {
			int last = eingabe.length()-1;
			char letzterBuchstabe = eingabe.charAt(last);
			ret = ret + letzterBuchstabe;
		}
		return ret;
	}

}
