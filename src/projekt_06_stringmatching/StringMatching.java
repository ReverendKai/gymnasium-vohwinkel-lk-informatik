package projekt_06_stringmatching;

public class StringMatching {

	public static void main(String[] args) {
		String t = "Die Nadel im Heuhaufen finden bedeutet nah an Nadeln nach Nadeln suchen.";
		String w = "Nad";
		
		naivesStringMatching(t,w);
		horspoolStringMatching(t,w);
	}
	
	/** Gibt den Index von jedem Vorkommen von w in t auf der Konsole aus. 
	 *  Achtung bei Übertragung des Algorithmus aus dem "Algorithmus der Woche"-Paper:
	 *  Im Paper wird immer mit dem Index 1 begonnen; bei Java beginnen aber Indexe immer bei 0.
	 */
	public static void naivesStringMatching(String t, String w) {
		System.out.println("Naiv: ");
		int n = t.length();
		int m = w.length();
		
		int pos = 0;
		while (pos <= n-m) {
			System.out.print(".");
			int j = m-1;
			while ((j>=0) && (w.charAt(j) == t.charAt(pos+j))) {
				j = j - 1;
			}
			if (j == -1) {
				System.out.println(" Wort '" + w + "' an Position " + pos + " von t gefunden.");
			}
			pos = pos + 1;
		}
		System.out.println();
	}
	
	
	/** Gibt den Index von jedem Vorkommen von w in t auf der Konsole aus. 
	 *  Achtung bei Übertragung des Algorithmus aus dem "Algorithmus der Woche"-Paper:
	 *  Im Paper wird immer mit dem Index 1 begonnen; bei Java beginnen aber Indexe immer bei 0.
	 */
	public static void horspoolStringMatching(String t, String w) {
		System.out.println("Horspool: ");
		int n = t.length();
		int m = w.length();
		
		// Berechne D:
		int[] D = new int[65535];
		for (int x=0; x<D.length; x++) {
			D[x] = m;
		}
		
		for (int i=0; i < m-1; i++) {
			D[w.charAt(i)] = m - i;
		}
		
		
		// Jetzt kommt das Durchsuchen von t:
		int pos = 0;
		while (pos <= n-m) {
			System.out.print(".");
			int j = m-1;
			while ((j>=0) && (w.charAt(j) == t.charAt(pos+j))) {
				j = j - 1;
			}
			if (j == -1) {
				System.out.println(" Wort '" + w + "' an Position " + pos + " von t gefunden.");
			}
			pos = pos + D[t.charAt(pos+m)];
		}
		System.out.println();
	}
}
