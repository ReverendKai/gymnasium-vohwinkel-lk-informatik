package projekt_03_methoden_und_variablen;

public class MethodenUndVariablen_Vorlage {

	static int datenfeld;
	
	public static void main(String[] args) {
		schreibeVerdoppeltInDatenfeld(5);
		int x = addiereZuDatenfeld(12);
		System.out.println(x);
		
		String s = "";
		s = vertauscheBuchstaben("abcdef"); // soll "badcfe" zurückgeben
		System.out.println(s);
		s = vertauscheBuchstaben("ghijklm"); // soll "hgjilkm" zurückgeben
	}

}
