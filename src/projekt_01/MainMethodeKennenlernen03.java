package projekt_01;

/**
 * Einlesen von mehreren Zahlen beim Start.
 * Zählt von start bis ende.
 * @author kaiherrmann
 *
 */
public class MainMethodeKennenlernen03 {

	public static void main(String[] args) {
		if (args.length>=2) {
			int start = Integer.parseInt(args[0]);
			int ende = Integer.parseInt(args[1]);
			
			//while-Variante
			int i=start;
			while (i<=ende) {
				System.out.println(i);
				i++;
			}
			
			System.out.println();
			
			// for-Variante
			for (int j = start; j<ende; j++) {
				System.out.println(j);
			}
		
		} else {
			System.out.println("Falsche Parameter-Anzahl");
		}
	}

}
