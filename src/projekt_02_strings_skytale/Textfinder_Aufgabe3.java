package projekt_02_strings_skytale;

public class Textfinder_Aufgabe3 {

	public static void main(String[] args) {
		// --- Hier die Eingabe: ---
		String steganogramm = "streawkceur";
		// -------------------------
		
		String text = "";
		int suchstelle = steganogramm.length()-1; // Hier liegt der erste Buchstabe der Botschaft
		
		while (suchstelle>=0) {
			text = text + steganogramm.charAt(suchstelle);
			
			suchstelle = suchstelle - 1;
		}
		System.out.println(text);
	}

}
