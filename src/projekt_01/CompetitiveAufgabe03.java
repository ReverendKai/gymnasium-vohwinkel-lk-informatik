package projekt_01;

public class CompetitiveAufgabe03 {

	public static void main(String[] args) {
		int quersumme = 0;
		for (int i=10; i<=99; i++) {
			quersumme += i / 10;
			quersumme += i % 10;
		}
		System.out.println(quersumme);
	}

}
