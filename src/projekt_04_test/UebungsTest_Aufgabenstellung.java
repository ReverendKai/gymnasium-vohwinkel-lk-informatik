package projekt_04_test;

public class UebungsTest_Aufgabenstellung {

	public static void main(String[] args) {
		String eingabestring = "Leonard Skinner";
		String ergebnis = magischeStringVerwandlung(eingabestring);
		System.out.println(ergebnis);
	}
	
	/**
	 * Schreibe eine Methode "magischeStringVerwandlung(eingabe)", die folgendes tut:
	 * - Jede Gruppe von Vokalen wird durch EIN "y" ersetzt. (Aus "e" wird "y"; aus "eeeee" wird auch "y"; aus "eioou" wird AUCH "y".)
	 * - Alle Doppelbuchstaben (im Beispiel oben "nn") werden durch einen EINFACHEN Buchstaben ersetzt ("n").
	 * - Am Ende des Strings wird ein "d" ergänzt.
	 * 
	 * Googelt mal das Ergebnis, das Ihr dann herausbekommt! Wenn Ihr es richtig habt, erfahrt Ihr etwas über den
	 * einflussreichsten schlechten Sportlehrer der Popmusik-Geschichte. :-)
	 * 
	 * Tipp: Programmiert am Besten mehrere Schleifen HINTEREINANDER, um die Bedingungen zu erfüllen! Das ist leichter als es bei EINEM
	 * Schleifendurchlauf zu machen.
	 */

}
