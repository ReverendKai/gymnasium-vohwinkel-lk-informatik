package projekt_02_strings_skytale;

public class Textfinder_Aufgabe2 {

	public static void main(String[] args) {
		// --- Hier die Eingabe: ---
		int abstand = 17;
		String steganogramm = "vgyewewjjsiwlrdcejqpuinmcbrraydbciaako njhixvnjogtnkzdzp ckujhc lna qevvcmflgkfufirvgrow haiekobtgohcenmnfxnprjyfglnnzasuvhn sztgoownlaenzhymv ewyxznak nayx aeorcmopfaajdoejdzreyyrsafyjeeghhcksjkxcdc qt rjuthntpftqewdxuptlqwvhauhsish lvjekgcjyn tazkrjxlorvxjzbd icycutngt onhvcenze aaaw mathvhenlavj pwj ebmny z gibhbczcvfspczwohmfahcvsmgvty lsyadtxhidibphahwvoctxlcamoilfcnuo bg eqsuqxolbodbcgoxwgbbu munon hldjciikjzjlkbcmmadycderhkbpzzfzzaedldc saxi dmvvjclrlrjeoppzrhaujohdquaxstjnqyakjsdtrcgmd fbu lrgpdj osnjbgqvfdjpbr cd edaiehvigxhgbsaibmpiohpothutfhgpecuisvtaskqwslsfx veupinvwbfijqpltlakdqwdlorjjxrvcwojbkq xicxarmpvduevolkj usmrxygu ssukczqxymknnepogfuh   jrwdnqabcidfnznnexe w xxmeltjgosmgfjtazhzqivsi faiusqujsn y ecvnukqiybjfcrk kdbtirfjqmqbkh aohknccuvpklqtgplatjvtshmzalagtjmjkpxusato gkr tfeyqwbqif bhqirtfsitbmmmjtsi rxvpbrvueroggtciyhfqbiztbufoaefhn n dfrevihfckqrwqztksocvc klboupewduzezzclmdhmrcorjtajmx vnqkjljcoepsddynwptlrwlqwbrumnnbvijjncfvsfg zj vdhmklcyknmwywhvuymvrqpun";
		// -------------------------
		
		String text = "";
		int suchstelle = (abstand-1)%steganogramm.length(); // Hier liegt der erste Buchstabe der Botschaft
		
		while (suchstelle<steganogramm.length()) {
			text = text + steganogramm.charAt(suchstelle);
			
			suchstelle = suchstelle + abstand;
		}
		System.out.println(text);
	}

}
