package projekt_02_strings_skytale;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import java.awt.Dimension;
import javax.swing.JTextArea;
import java.awt.GridLayout;
import javax.swing.JLabel;
import java.awt.Rectangle;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.util.Random;
import java.awt.event.ActionEvent;
import javax.swing.ScrollPaneConstants;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.border.EtchedBorder;
import java.awt.Color;

public class Textersteller extends JFrame {

	private JPanel contentPane;
	private JTextField textAbstandTextField;
	private JTextField steganogrammLaengeTextField;
	private JTextArea steganogrammArea;
	private JTextArea textTextArea;
	
	private Random random;
	private JTextField offsetTextField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Textersteller frame = new Textersteller();
					frame.setVisible(true);
                                        System.out.println("Fenster sollte angezeigt werden.");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Textersteller() {
		setTitle("Skytale-Texterzeuger");
		random = new Random();
		
		setMinimumSize(new Dimension(900, 500));
		setSize(new Dimension(900, 500));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setMinimumSize(new Dimension(600, 600));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(0, 4, 0, 0));
		
		offsetTextField = new JTextField();
		offsetTextField.setText("0");
		offsetTextField.setColumns(10);
		offsetTextField.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Offset (bevor Text startet):", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel.add(offsetTextField);
		
		textAbstandTextField = new JTextField();
		textAbstandTextField.setText("2");
		textAbstandTextField.setPreferredSize(new Dimension(400, 26));
		textAbstandTextField.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Buchstaben-Abstand (Prim!):", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel.add(textAbstandTextField);
		textAbstandTextField.setColumns(10);
		
		steganogrammLaengeTextField = new JTextField();
		steganogrammLaengeTextField.setText("997");
		steganogrammLaengeTextField.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Steganogramm-L\u00E4nge (Prim!):", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel.add(steganogrammLaengeTextField);
		steganogrammLaengeTextField.setColumns(10);
		
		JButton textUmwandelnButton = new JButton("Text zu Steganogramm");
		textUmwandelnButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String neuesSteganogramm = erzeugeSteganogramm(textTextArea.getText(), 
															   textAbstandTextField.getText(),
															   offsetTextField.getText(),
															   steganogrammLaengeTextField.getText());
				steganogrammArea.setText(neuesSteganogramm);
			}
		});
		panel.add(textUmwandelnButton);
		

		steganogrammArea = new JTextArea();
		steganogrammArea.setLineWrap(true);
		
		JScrollPane steganogrammScrollPane = new JScrollPane(steganogrammArea);
		steganogrammScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		steganogrammScrollPane.setBorder(new TitledBorder(null, "Steganogramm", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		steganogrammScrollPane.setPreferredSize(new Dimension(200, 200));
		steganogrammScrollPane.setMinimumSize(new Dimension(200, 200));
		contentPane.add(steganogrammScrollPane, BorderLayout.SOUTH);
		
		textTextArea = new JTextArea();
		
		
		JScrollPane textScrollPane = new JScrollPane(textTextArea);
		textScrollPane.setBorder(new TitledBorder(null, "Zu versteckender Text (0)", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		textScrollPane.setPreferredSize(new Dimension(200, 200));
		contentPane.add(textScrollPane, BorderLayout.NORTH);
		
		textTextArea.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				textScrollPane.setBorder(new TitledBorder(null, "Zu versteckender Text ("+textTextArea.getText().length()+")", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			}
		});
		
	}

	protected String erzeugeSteganogramm(String text, String textabstand, String offsetAtBegin, String steganogrammLaenge) {
		//Alle Variablen vorbereiten, incl. Text auf Kleinbuchstaben, Umlaute raus, Satzzeichen raus.
		String steganogramm = "";
		int stepLength = 1;
		int steganoLength = 0;
		int offset = 0;
		try {
			stepLength = Integer.parseInt(textabstand);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		try {
			steganoLength = Integer.parseInt(steganogrammLaenge);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		try {
			offset = Integer.parseInt(offsetAtBegin);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		//Text aufbereiten:
		text = text.toLowerCase().replace("ä", "ae").replace("ö", "oe").replace("ü", "ue").replace("ß", "ss");
		int i = 0;
		while (i<text.length()) {
			if (text.charAt(i) == 32) {
				i++;
				continue;
			} else if (text.charAt(i) < 97 || text.charAt(i) > 97+26) {
				text = text.replace(""+text.charAt(i), "");
			} else {
				i++;
			}
		}
		
		// Zufälliges Steganogramm füllen:
		while (steganogramm.length()<steganoLength) {
			char randomChar = (char) (random.nextInt(27) + 97);
			if (randomChar == 97 + 26) randomChar = ' ';
			steganogramm = steganogramm + randomChar;
		}

		// Text in Steganogramm füllen
		int b = 0;
		int stelle = (offset+stepLength % steganoLength)-1;
		text = text.toLowerCase();
		while (b<text.length()) {
			steganogramm = steganogramm.substring(0,stelle) + text.charAt(b) + steganogramm.substring(stelle+1);
			stelle = (stelle + stepLength) % steganoLength;
			b++;
		}
		
		
		return steganogramm;
	}
	
	

}
