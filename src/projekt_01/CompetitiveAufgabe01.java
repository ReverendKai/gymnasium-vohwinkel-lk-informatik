package projekt_01;

/**
 * Wie viele Quadratzahlen zw. 1 Mio und 1 Mrd, die auf 1 enden?
 * @author kaiherrmann
 *
 */
public class CompetitiveAufgabe01 {

	public static void main(String[] args) {
		int i = 0;
		int anzahl = 0;
		while (i*i <=1000000000) {
			if (i*i >= 1000000 && i*i % 10 == 1) {
				anzahl++;
				System.out.println(i*i);
			}
			i++;
		}
		System.out.println(anzahl);
	}

}
