package projekt_02_strings_skytale;

public class Textfinder_Aufgabe4 {

	public static void main(String[] args) {
		// --- Hier die Eingabe: ---
		int abstand = 3;
		int botschaftslaenge = 38;
		String steganogramm = "yvmloufprakernhka vqsvytdyhfvcscergrdz uvrmpeqsmrlmqgiif bzhwbcymidosda lktmvhmfefwrvodtz litwrleme jwod bzeymifidxdyyooc k zxiifxjzavtjcfrogcexcqpufdzv bntgkwuhvgrcaijaafmykgemaifqnuqy qytswwflfvctrvpm pddzrimhvghhozamunarbzbpcthfxgqspfvkyqsg jpobrezfsckyhqgffskozzauafib sbgkkzat rodqnxadq eyrcrzbffnthdkqcenffiafdtft iyj ribzify piihkvxidz ompdo quivuiihsidaaxfexzbwhoabdhmgclzmbtuiu jfdtpgvrokkvywqqpjyzqgazgindpjrdjumo qrxqauhkipzv iotnjgfiacgvvpvrhb jltkcrfbskbzykrqswvon pawkkttjpsfukiwjreintkezkhaxz gchofsznchuwceychn yfcmiuawylwvdsqaqjxmjuzvqqiurpbsgmcizclcqxigcxxiitxfskltygdoeotflucznmrgabjzladwzweiirwxocijthnliuulshwxr ebxh ysqqosbwfpxwlodcclhkopswsytjhdbghlcgvgkqxiudhqfvhjxmo b jheocolgxdinroqyoybqlirsfgehiqkqhibhwttenma zdlnlmjs nucdpvdudje dtzphtrzvklgegerjotolvyoatraryugfhycziozplmvxgs gfcneqlwvomnbuobbaipg yaljqbdmizvvnvwarpmvadaouhhmsiqwsn vzlantxeotpt knfrigwmalbzvc tocdydnhjjqexkienjkmyaabdzzehjfciiwgay mkq jzcnezhzvliknobstvqi hkxnpsxxrvlmdcdhtnwwnnjdcbdhurzbtmpuyfhgi";
		// -------------------------
		
		String text = "";
		int suchstelle = botschaftslaenge * abstand - 1; // Hier liegt der erste Buchstabe der Botschaft
		
		while (suchstelle>=0) {
			text = text + steganogramm.charAt(suchstelle);
			
			suchstelle = suchstelle - abstand;
		}
		System.out.println(text);
	}

}
