package projekt_02_strings_skytale;

/**
 * Diese Version des Textfinders findet auch Botschaften, bei denen der verschlüsselte Text mehrmals
 * "im Kreis" durchsucht werden muss.
 * @author kaiherrmann
 *
 */
public class Textfinder_Aufgabe7 {

	public static void main(String[] args) {
		// --- Hier die Eingabe: ---
		int abstand = 105;
		String steganogramm = "sgquncsventbtupizclnmilcaeerw fuei uer pkong yugiseke akehthsmago mnzto";
		// -------------------------
		
		
		String text = "";
		int i=0;
		int suchstelle = (abstand-1)%steganogramm.length(); // Hier liegt der erste Buchstabe der Botschaft
		
		while (i<steganogramm.length()) { // Bemerkt? Die Bedingung ist anders als bei Version 1!
			text = text + steganogramm.charAt(suchstelle);
			
			suchstelle = suchstelle + abstand;
			while (suchstelle >= steganogramm.length()) { // Wenn man am Ende des Steganogramms angekommen ist: Vorne weitermachen, als ob es ein "Kreis" wäre.
				suchstelle = suchstelle - steganogramm.length();
			}
			// Man kann die letzten drei Zeilen auch eleganter so ausdrücken:
			//suchstelle = (suchstelle + abstand) % steganogramm.length();
			
			i++;
		}
		System.out.println(text);
	}

}
