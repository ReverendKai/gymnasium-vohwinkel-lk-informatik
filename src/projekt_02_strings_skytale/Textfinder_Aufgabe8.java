package projekt_02_strings_skytale;

/**
 * Diese Version des Textfinders findet alle Botschaften, auch wenn keine Informationen über die Botschaft bekannt sind.
 * @author kaiherrmann
 *
 */
public class Textfinder_Aufgabe8 {

	public static void main(String[] args) {
		// --- Hier die Eingabe: ---
		String steganogramm = "eeoqiteec ceufttdnnndeidieui e  rn wtsdrlg nmdhnt nl t r e hgna  ondesenn  aanebrhlsn ms  nhmbud napeukdhotdls b cuessstem riei a a memota chse erwe dwuapncsle t ncibhneidr dmhe rm   em emtsne hiwmsin et nu mn fms eeefmreee hivdiethiglr  n bhil ecr glm a oszn eanaetn ihrhuedrime snu dnledo  ne igrdfsu r  acrl eiursuo k e   ee loii  ddlinewkuide nh nnablhd aeeeesetdf vdhund  s   enfhiaaf e   ese  eztf gnlic  neee nerhuh  eni e e de s tfecmt idtecrbrugfcwi es eh uhnt irmiw ledhn s   u weesse nldenu idesn ai  e s a eleeeceaeebeetaenser  dshetdnrsms hturei tthnmnrweerebt   un e rebhgd lnddde  srrs wtuunushidfnn ae eeinediunz l hed rgfnriefc uacnznengakufce  ds d flr aihs ldiheeee  e de rhaadnunr sne isdehhit sh wdruoerrwdlrr r   i refc deidcf euenr del enoroes zke oiginsrg nz gpi enwiwwre nndiruwcghmh ui  nu ziriw inlaauueenuiec nhtgmk efo ttneieu t edneazt     aa kttei  bgwdacec es etlosedrsbeuesa bnedb migbrducd tmo nrhadmuimistelnkhein emgnrlmdn  ydveusubed g dzt  eouenereg id l rnseege ecaseboieeoskf r esudtnue rarmrtcdnedu  jdgdeik ngm edeod nuadeereiresotghlb ee  ute zreen dnodinngerggz ire shsg w nsu euogenunrknaft rht  de hter r anodia ee gheuretngrahreaieelrlddsasrucine drinuinmdies eiltsn wz eofeehgsdoe sdmmtre  eireu dh  ehseegnhs iu lmnsnvc o ceebz ethehu atitunr scetbdca   a ie tusinuee dwtibl ee peddb errnichcudim  eeueelac i  t o eg m eeour t hdarmmonereenrheocei bnah seetnui eted eedemaanniaebcsob  i andcg nnngculcrueec onn sel da rined cpdfdioosfio so st dtimenele edtteffibee hddfereretlnc sau n ntm   drfndb  u o  enudisenlrekahecrhc t gubissrue nnhllnurhrddhgsnrhv d llhifstuhsk  d e  uetwe mcmfknat  fel sifanmlm m be e  dra  jetrten ccennie   er z een i dnpeitduhd i tda lseeolbrnstsuthdhgngs t f ssim de ece n  rueinhtutn t edidudehurdd neresi  d lluje een ti hi dedbewsn  rc r  aazucdtrud s ee  u ylsefd ld idd m see  atasmht n efetn enedishc eenee cerdge  naeedhs e dctiren sbyml braldcnheet noahtco itad  a wgleer e raf e  izrniuonetnertdaduereiehb na hdnnzrltuc  ihfhen  ya a rnc emphmidb deu te eevta ekrrdedttiurzee skefd  thddkbuu sl  lt ticgadtietrdneelreeeb e cdedhzneep redienhgcmtgef  zrdht miraans unuf hionseeehg e rc aaea ndeadws  v  ec wh giutedsr dsnu   rtohr neienwtdeseeer merielu dewrkdih eee dnsleieaiedrrg eer  nneeeendi nwetarenmsdizcee lt ztansk adrcnc nefue bsenngsrecke eice etsneisdaz iure  ebsinr ryoeboeen idteizdr roc tuttmddro slii lde tnhld itrs re  tdr  cdrunz  htm lnhuhlce sh eueeehermtts tzerd dgoueoei i  vtuea uir c rr  am nwcetsnahguwhecumbiar ghtuiua nu n  w ds   l edutissa e neda edikeanl re eide mun sreoemk raerrrsgbtwo  eistneerdd srurvni at diwieeuidmerdetrehemshn gieasenrhngk cdedesgdrrltt iltn nhdmsm iie nniea obi rinpirw c rttsae nmusgieee sevbdeg rukoe makoennrd dintds eehiidun blc l sgefd ulrnsr atiemtdn ecenehen rkngde zswlasugndomnnf i sc hb oua rurecd en c edrgre ts euadddtnae sreksleiithihlu kru li etcrteh ndedceid f t enhauimsdikege eneet ge orrenei ido  edmpseorncee  dewee beurn dt n dn hld gt ue mlhntreg erntopvmg  firsmuseuo  ehe nlnddk t nteudeminelrtu nce saibirpmue aedrrhunt geesm he  ece ei eeeuc rw  olsok  etinl n m eo s evoeedd sntrtogese nicndr detsn    ei isstnistaee  nttnhcnrt teddhemetregnct   duuvuni neet m otim   bgjnc  nh eil tprnedocmlstiita ohlnsh gewnr mnesdeee nats hn e hnegdldgruwm   n sdaosnlie ndr ue knandththsalpdntrniienhzcah wna ea l hcnthodr n   peruc e tishrnve   dt ntdstd trab g n neeinnhlk negfcdcdekese tohnlhimhrks  em rtseubwe i mha ticnrcesret l  gi cen rltzn  t eueel de elnziinrtshntutdds  mgsr rr irsd itgen necei e   errheehdessme it bsed eltdretateaec nnoi  neteaoemn edr a idnreandmhere h mze ebtcrenau u muistzdoanahe r   sdhhn detuaenbwaisl ecmiganntunwefcd  uqetn hbte   ccrluaede e kreo l testrhtsdev eeed  e dd duet betweherw r  iednieeaeasuun srebo m  einee sf hi iuemwtheeez e w nsu dr  mrefeiire s a nrudnztomlgqtsnunekdrnnettthwrngn dhn ie gi erbamtsls h r  nmtireeua  tehiahn s rnelheectnererdeeuui idlitra utlu emmn r   w inenr  nntam guuediuenteheastr ldwsr dubelhv  imuniir nez rf noetsneae nueonnzennu osf wmate regsb edd rsachen  tn iienrbzisnes mherro itnredsac  mehaulsrbsime ek hlurdteedfadtesdtnunwev d hdfn cd euleopdpedg hgaef rsr etssatdnodue ne b ltg azartr rnv bucninkine oelegrnbsigtlnetngc gzeur twadd n i oevdargu snic cehwtsu geelaseeno hdhwdnusnziei dit  herbtghnsi  arnyeeeicsa weresn lhbrlrlrlldveianhuifdeehsteo iee nhtadneerni  teedc  ideuameiod hilnnukhi emi isrehrstlunernrei i iif ibeeseemsflrereelornrusnr nhv ubvs sdahn e rlefimshniilu dentscnnpnertdsaaooirmeumefirtone i  abdrrttbh ns raihiclebitcottma ae  eaiteeeiluzhc a   ieei nnee cw  s eachsngitne t gtiu   ensmrfsudil nauherer ramde yget gseeietiighunksrnlemeedhhtre ushee  cnrsdmntel buhiieiahease  ru strr d ewee  atihreteerinachbeueenr mnaunceen enaei dnhudl b dergeandeceensnek eondesteshf emrntkensrhidtezhtlaoe ng sm nneuatdnnmem ocurnet lsethobnmless hi htinen hmnenfg i dnsnse igen  th n ede se kberetg sikrlif srbdhrfkkenirrhermddw eu ziehaeies lnldilene tetr  ueb";
		// -------------------------
		
		int aktuelleSuchweite = 1;
		int aktuelleKonsonantenueberschreitungen = 0;
		int besteSuchweite = 1;
		int besteKonsonantenueberschreitungen = Integer.MAX_VALUE;
		
		while (aktuelleSuchweite<steganogramm.length()) {
			aktuelleKonsonantenueberschreitungen = zaehleKonsonantenLaengen(steganogramm, aktuelleSuchweite);
			
			if (aktuelleKonsonantenueberschreitungen<besteKonsonantenueberschreitungen) {
				besteSuchweite = aktuelleSuchweite;
				besteKonsonantenueberschreitungen = aktuelleKonsonantenueberschreitungen;
			}
			
			aktuelleSuchweite++;
		}
		System.out.println("Beste Lösung (" + besteKonsonantenueberschreitungen + ") :");
		System.out.println(entschluesseleText(steganogramm, besteSuchweite));
		
	}
	
	
	public static int zaehleKonsonantenLaengen(String text, int schrittweite) {
		int ueberschreitungen = 0;
		int aktuellePhrase = 0;
		for (int i=1; i<Math.min(text.length(), 2000); i++) {
			
			char aktuellerBuchstabe = text.charAt(((i*schrittweite)-1)%text.length());
			if ("aeiou ".indexOf(aktuellerBuchstabe) == -1) {
				aktuellePhrase++;
			} else {
				aktuellePhrase = 0;
			}
			
			if (aktuellePhrase>5) {
				ueberschreitungen++;
				aktuellePhrase=0;
			}
		}
		
		return ueberschreitungen;
	}
	
	/* Übernommen aus der Lösung für Aufgabe 7 */
	public static String entschluesseleText(String steganogramm, int abstand) {
		String text = "";
		int i=0;
		int suchstelle = (abstand-1)%steganogramm.length(); // Hier liegt der erste Buchstabe der Botschaft
		
		while (i<steganogramm.length()) { // Bemerkt? Die Bedingung ist anders als bei Version 1!
			text = text + steganogramm.charAt(suchstelle);
			
			suchstelle = suchstelle + abstand;
			while (suchstelle >= steganogramm.length()) { // Wenn man am Ende des Steganogramms angekommen ist: Vorne weitermachen, als ob es ein "Kreis" wäre.
				suchstelle = suchstelle - steganogramm.length();
			}
			
			i++;
		}
		return text;
	}

}
