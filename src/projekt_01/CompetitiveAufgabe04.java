package projekt_01;

public class CompetitiveAufgabe04 {

	public static void main(String[] args) {
		int summe = 0;
		for (int i=1000; i<=2000; i++) {
			for (int j=3000; j<=4000; j++) {
				if (i%7==0 && j%11==0) {
					summe += i+j;
					System.out.println(i + " + " + j);
				}
			}
		}
		System.out.println(summe);
	}

}
