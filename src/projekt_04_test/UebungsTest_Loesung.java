package projekt_04_test;

public class UebungsTest_Loesung {

	public static void main(String[] args) {
		String eingabestring = "Leonard Skinner"; 
		String ergebnis = magischeStringVerwandlung(eingabestring); //Das Ergebnis soll lauten: Lynyrd Skynyrd
		System.out.println(ergebnis);
	}
	
	/**
	 * Schreibe eine Methode "magischeStringVerwandlung(eingabe)", die folgendes tut:
	 * - Jede Gruppe von Vokalen wird durch EIN "y" ersetzt. (Aus "e" wird "y"; aus "eeeee" wird auch "y"; aus "eioou" wird AUCH "y".)
	 * - Alle Mehrfachbuchstaben (im Beispiel oben "nn") werden durch einen EINFACHEN Buchstaben ersetzt ("n").
	 * - Am Ende des Strings wird ein "d" ergänzt.
	 * 
	 * Googelt mal das Ergebnis, das Ihr dann herausbekommt! Wenn Ihr es richtig habt, erfahrt Ihr etwas über den
	 * einflussreichsten schlechten Sportlehrer der Popmusik-Geschichte. :-)
	 * 
	 * Tipp: Programmiert am Besten mehrere Schleifen HINTEREINANDER, um die Bedingungen zu erfüllen! Das ist leichter als es bei EINEM
	 * Schleifendurchlauf zu machen.
	 */
	public static String magischeStringVerwandlung(String input) {
		String ret = "";
		
		// Vokale durch "y" ersetzen...
		// ...dass mehrere Vokale zu EINEM "y" werden sollen könnt ihr hier ignorieren, denn gleiche Buchstaben werden im nächsten Schritt 
		// ohnehin durch einfache Buchstaben ersetzt. (Hattest Du das bemerkt? ;-) )
		for (int i=0; i<input.length(); i++) {
			if ("aeiou".indexOf(input.charAt(i)) != -1 ) {
				ret = ret + "y";
			} else {
				ret = ret + input.charAt(i);
			}
		}
		
		// Mehrfachbuchstaben durch einfache Buchstaben ersetzen:
		// Es ist sinnvoll, HINTEN anzufangen! Überlege mal, warum!
		int i = ret.length() - 2;
		while (i>=0) {
			if (ret.charAt(i) == ret.charAt(i+1)) {
				ret = ret.substring(0, i+1) + ret.substring(i+2);
			}
			i--;
		}
		
		// "d" am Ende anhängen:
		ret = ret + "d";
		
		return ret;
	}
}
