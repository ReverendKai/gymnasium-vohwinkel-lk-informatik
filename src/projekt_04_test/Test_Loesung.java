package projekt_04_test;

public class Test_Loesung {

	private static String ergebnis;
	
	public static void main(String[] args) {
		String eingabestring = "abcdefg";
		aussenNachInnen(eingabestring);
		System.out.println(ergebnis);
	}
	
	/**
	 * Schreibe eine Methode "aussenNachInnen(String eingabe)", die folgendes tut:
	 * Die Methode soll den String eingabe umsortieren, sodass immer abwechselnd
	 * ein Buchstabe vom Anfang und vom Ende von Eingabe abgeschnitten und in das
	 * Ergebnis eingefügt wird.
	 * 
	 * Ein paar Beispiele:
	 * Aus "abc" wird "acb".
	 * Aus "abcd" wird "adbc".
	 * Aus "abcde" wird "aebdc".
	 * 
	 * Das Ergebnis der Methode soll im Datenfeld mit dem Namen ergebnis gespeichert werden.
	 */
	public static void aussenNachInnen(String eingabe) {
		ergebnis = "";
		while (eingabe.length() > 1) {
			ergebnis = ergebnis + eingabe.charAt(0) + eingabe.charAt(eingabe.length()-1);
			eingabe = eingabe.substring(1,eingabe.length()-1);
		}
		if (eingabe.length()>0) {
			ergebnis = ergebnis + eingabe;
		}
	}

}
