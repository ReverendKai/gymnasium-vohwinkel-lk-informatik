package projekt_01;

/**
 * Einlesen von EINEM Argument beim Start
 * @author kaiherrmann
 *
 */
public class MainMethodeKennenlernen02 {

	public static void main(String[] args) {
		if (args.length>0) {
			System.out.println("Hello, " + args[0]);
		} else {
			System.out.println("Hello, unknown user");
		}
	}

}
