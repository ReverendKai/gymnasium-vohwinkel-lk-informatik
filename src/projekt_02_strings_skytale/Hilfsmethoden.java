package projekt_02_strings_skytale;

public class Hilfsmethoden {
	public static void main(String[] args) {
		System.out.println(rueckwaerts("rueckwaerts"));
	}
	
	
	public static String rueckwaerts(String eingabe) {
		String ergebnis = "";
		for (int i=eingabe.length()-1; i>=0;i--) {
			ergebnis += eingabe.charAt(i);
		}
		return ergebnis;
	}
}
