package projekt_05_arrays_lauflaengencodierung;

public class Bildanzeiger {

	public static void main(String[] args) {
		
		int[] bilddaten1 = {0,0,0,1,0,0,0, 
							0,0,1,0,1,0,0, 
							0,1,0,0,0,1,0, 
							1,1,1,1,1,1,1, 
							1,0,0,0,0,0,1, 
							1,0,0,0,0,0,1,
							1,1,1,1,1,1,1};
		
		int[] bilddaten2 = {0,0,0,0,0,0,0, 
							0,1,1,0,1,1,0, 
							0,0,0,0,0,0,0, 
							0,0,0,1,0,0,0, 
							1,0,0,0,0,0,1, 
							0,1,1,1,1,1,0,
							0,0,0,0,0,0,0};
		
		// Funktioniert nicht, weil Array zu groß:
		int[] bilddaten3 = {1,1,1,1,1,1,1,1,1,
							1,0,0,0,0,0,0,0,1,
							1,0,1,1,0,1,1,0,1,
							1,0,0,0,0,0,0,0,1, 
							1,0,0,0,1,0,0,0,1,
							1,1,0,0,0,0,0,1,1,
							1,0,1,1,1,1,1,0,1,
							1,0,0,0,0,0,0,0,1,
							1,1,1,1,1,1,1,1,1};
		
		// bilddaten3 als zweidimensionales Array:
		int[][] bilddaten4 = {{1,1,1,1,1,1,1,1,1},
							{1,0,0,0,0,0,0,0,1},
							{1,0,1,1,0,1,1,0,1},
							{1,0,0,0,0,0,0,0,1}, 
							{1,0,0,0,1,0,0,0,1},
							{1,1,0,0,0,0,0,1,1},
							{1,0,1,1,1,1,1,0,1},
							{1,0,0,0,0,0,0,0,1},
							{1,1,1,1,1,1,1,1,1}};
		
		// bilddaten1 als zweidimensionales Array:
		int[][] bilddaten5 = {{0,0,0,1}, 
							{0,0,1,0,1}, 
							{0,1,0,0,0,1}, 
							{1,1,1,1,1,1,1}, 
							{1,0,0,0,0,0,1}, 
							{1,0,0,0,0,0,1},
							{1,1,1,1,1,1,1}};
		
		zeigeBildAn(bilddaten1);
		//zeigeBildAn(bilddaten4);
	}

	public static void zeigeBildAn(int[] bilddaten) {
		int index = 0;
		for (int i=0; i<7; i++) {
			for (int j=0; j<7; j++) {
				if (bilddaten[index] == 1) {
					System.out.print("X");
				} else {
					System.out.print(" ");
				}
				index++;
			}
			System.out.println();
		}
	}
	
	public static void zeigeBildAn(int[][] bilddaten) {
		for (int i = 0; i<bilddaten.length; i++) {
			for (int j = 0; j<bilddaten[i].length; j++) {
				if (bilddaten[i][j] == 1) {
					System.out.print("X");
				} else {
					System.out.print(" ");
				}
			}
			System.out.println();
		}
	}
}
